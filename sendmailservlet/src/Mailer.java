
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

class Mailer {
	public void send(String recipient, String path, String f_name) throws Exception {
		try {
			final String fromEmail = "firojkcsjm0805@gmail.com"; // requires
																	// valid
																	// gmail id
			final String password = "nasreen123"; // correct password for gmail
													// id
			// final String toEmail = "shabankhan000749@gmail.com"; // can be
			// any email id

			System.out.println("TLSEmail Start");
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com"); // SMTP Host
			props.put("mail.smtp.port", "587"); // TLS Port
			props.put("mail.smtp.auth", "true"); // enable authentication
			props.put("mail.smtp.starttls.enable", "true"); // enable STARTTLS

			// create Authenticator object to pass in Session.getInstance
			// argument
			Authenticator auth = new Authenticator() {
				// override the getPasswordAuthentication method
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(fromEmail, password);
				}
			};
			Session session = Session.getInstance(props, auth);
			MimeMessage message = new MimeMessage(session);

			setRecipientsForReport(message, fromEmail, recipient);
			setSubjectAndDate(message);
			MimeBodyPart messageBodyPart = new MimeBodyPart();

			String str = setMailContent();
			messageBodyPart.setContent(str, "text/html");
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			MimeBodyPart attachmentPart = createAttachment(path, f_name);
			multipart.addBodyPart(attachmentPart);

			message.setContent(multipart);
			Transport.send(message);
			System.out.println("Mail Sent");
		} catch (Exception ex) {
			System.out.println("Mail fail");
			System.out.println(ex);
		}
	}

	private MimeBodyPart createAttachment(String path, String f_name) throws Exception {
		System.out.println("Inside createAttachment mmethod,Going for attachment");
		MimeBodyPart attachmentPart = new MimeBodyPart();
		String filename = path;
		DataSource source = new FileDataSource(filename);
		attachmentPart.setDataHandler(new DataHandler(source));
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		String fileDate = sdf.format(new Date());
		String displayName = f_name;
		attachmentPart.setFileName(displayName);
		System.out.println("File Attached Successfully.");
		return attachmentPart;
	}

	public String setMailContent() {
		String html = "";
		html += "<div style=\"color:black;\">" + "Hi," + "</div>";
		html += "<br>";
		html += "<div style=\"color:black;\">" + "This is a testing message" + "</div>";
		html += "<br>";
		html += "<div style=\"color:black;\">" + "Thanks," + "</div>";
		html += "<div style=\"color:black;\">" + "Firoj Khan" + "</div>";
		return html;
	}

	private void setSubjectAndDate(MimeMessage msg) throws Exception {
		msg.setSubject("Testing Message From Mail Application");
	}

	private void setRecipientsForReport(MimeMessage msg, String from, String recipient) throws Exception {

		msg.setFrom();
		InternetAddress fromAddress = new InternetAddress(from);
		InternetAddress toAddress1 = new InternetAddress(recipient);
		msg.setFrom(fromAddress);
		msg.addRecipient(Message.RecipientType.TO, toAddress1);

	}
}
